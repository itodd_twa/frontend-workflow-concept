/* eslint strict: 0, dot-notation: 0, no-console: 0 */

// *************************************
//
//   Gulpfile
//
// *************************************

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var path = require('path');

// -------------------------------------
//   Options
// -------------------------------------
var options = {

    // Default
    default: {
        tasks: ['assets:fonts', 'assets:images', 'js:lib', 'js:main', 'js:components', 'css:lib', 'css:main', 'watch']
    },

    // Build
    build: {
        tasks: ['assets:fonts', 'assets:images', 'js:lib', 'js:main', 'js:components', 'css:lib', 'css:main']
    },

    // Assets
    assets: {
        fonts: {
            destination: 'dist/assets/fonts',
            files: 'src/common/assets/fonts/**/*.*'
        },
        images: {
            destination: 'dist/assets/img',
            files: 'src/common/assets/img/**/*.*'
        }
    },

    // CSS
    css: {
        destination: {
            lib: 'dist/styles/lib',
            main: 'dist/styles'
        },
        files: {
            lib: 'src/common/styles/lib/lib.scss',
            main: 'src/common/styles/main.scss',
            finalMain: function() {
                return options.css.destination.main + '/main.css';
            }
        },
        lint: ['src/scss/**/*.scss', '!src/scss/lib/*.scss'],
        prefixes: ['> 2%']
    },

    // HTML
    html: {
        files: '*.html'
    },

    // JavaScript
    js: {
        destination: {
            lib: 'dist/scripts/lib',
            main: 'dist/scripts',
            components: 'dist/scripts/components'
        },
        files: {
            lib: ['src/common/scripts/lib/jquery.js', 'src/common/scripts/lib/*.js'],
            libNoCompile: ['src/common/scripts/lib/no_compile/*.js'],
            main: ['src/common/scripts/main.js', 'src/common/scripts/**/*.js', 'src/components/**/*.js', '!src/common/scripts/lib/**/*.js'],
            components: ['src/components/**/*.js']
        },
        lint: ['Gulpfile.js', 'src/common/scripts/**/*.js', 'src/components/**/*.js', '!src/common/scripts/lib/**/*.js']
    },

    lint: {
        errors: {}, // Object to store lint errors
        tasks: {
            all: ['lint:html', 'lint:js', 'lint:sass']
        }
    },

    hooks: {
        'pre-commit': ['lint:js:hook', 'lint:sass:hook']
    },

    // Watch
    watch: function() {
        return [

            // Watch JS
            {
                files: options.js.lint,
                tasks: ['js:main']
            },

            // Watch Sass
            {
                files: options.css.lint,
                tasks: ['css:main']
            }
        ];
    }
};

// -------------------------------------
//   Task: Default
// -------------------------------------

gulp.task('default', options.default.tasks);

// -------------------------------------
//   Task: Build
// -------------------------------------

gulp.task('build', options.build.tasks);

// -------------------------------------
//   Task: Assets Create Fonts
// -------------------------------------

gulp.task('assets:fonts', function() {
    return gulp.src(options.assets.fonts.files)
        .pipe(gulp.dest(options.assets.fonts.destination));
});

// -------------------------------------
//   Task: Assets Create Images
// -------------------------------------

gulp.task('assets:images', function() {
    return gulp.src(options.assets.images.files)
        .pipe(gulp.dest(options.assets.images.destination));
});

// -------------------------------------
//   Task: Generate CSS - Lib
// -------------------------------------

gulp.task('css:lib', function() {
    var _dest = options.css.destination.lib;

    return plugins.rubySass(options.css.files.lib, { style: 'expanded' })
        .pipe(gulp.dest(_dest))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(_dest));
});

// -------------------------------------
//   Task: Compile Sass - Main
// -------------------------------------

gulp.task('compile:sass:main', function() {
    return plugins.rubySass(options.css.files.main, {
        style: 'expanded',
        sourcemap: true
    })
        .pipe(plugins.autoprefixer({
            browsers: options.css.prefixes
        }))
        .pipe(plugins.sourcemaps.write('/'))
        .pipe(gulp.dest(options.css.destination.main));
});

// -------------------------------------
//   Task: Gerenrate CSS - Main
// -------------------------------------

gulp.task('css:main', ['lint:sass', 'compile:sass:main'], function() {
    return gulp.src(options.css.files.finalMain())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(options.css.destination.main));
});

// -------------------------------------
//  Task: Lint Sass - Main
// -------------------------------------

function lintSass(isHook) {
    return gulp.src(options.css.lint)
        .pipe(plugins.scssLint({
            config: '.scss-lint.yml',
            maxBuffer: 9999999
        }))
        .pipe(plugins.if(isHook, plugins.scssLint.failReporter()));
}

gulp.task('lint:sass', function() {
    return lintSass();
});

// -------------------------------------
//  Task: Lint Sass - Main for Git Hook
// -------------------------------------

gulp.task('lint:sass:hook', function() {
    return lintSass(true);
});

// -------------------------------------
//   Task: Generate JS - Lib
// -------------------------------------

gulp.task('js:lib', ['js:lib:nocompile'], function() {
    var _dest = options.js.destination.lib;

    return gulp.src(options.js.files.lib)
        .pipe(plugins.concat('lib.js'))
        .pipe(gulp.dest(_dest))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(gulp.dest(_dest));
});

// -------------------------------------
//   Task: Generate JS - Lib No Compile
// -------------------------------------

gulp.task('js:lib:nocompile', function() {
    var _dest = options.js.destination.lib;

    return gulp.src(options.js.files.libNoCompile)
        .pipe(gulp.dest(_dest))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(gulp.dest(_dest));
});

// -------------------------------------
//   Task: Generate JS - Main
// -------------------------------------

gulp.task('js:main', ['lint:js'], function() {
    var _dest = options.js.destination.main;

    // If JS lint has errors don't build JS
    if (options.lint.errors.js > 0) return false;

    return gulp.src(options.js.files.main)
        .pipe(plugins.concat('main.js'))
        .pipe(gulp.dest(_dest))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(gulp.dest(_dest));
});

// -------------------------------------
//   Task: Generate JS - Components
// -------------------------------------
gulp.task('js:components', ['lint:js'], function() {
    var _dest = options.js.destination.components;

    // If JS lint has errors don't build JS
    if (options.lint.errors.js > 0) return false;

    return gulp.src(options.js.files.components)
        .pipe(gulp.dest(function(file) {
            file.path = file.base + path.basename(file.path);
            return _dest;
        }))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(file) {
            file.path = file.base + '/' + path.basename(file.path);
            return _dest;
        }));
});

// -------------------------------------
//   Task: Lint JS
// -------------------------------------

function lintJs(isHook) {
    return gulp.src(options.js.lint)
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format())
        .pipe(plugins.eslint.results(function(results) {
            options.lint.errors.js = results.errorCount;
        }))
        .pipe(plugins.if(isHook, plugins.eslint.failAfterError()));
}

gulp.task('lint:js', function() {
    return lintJs();
});

// -------------------------------------
//   Task: Lint JS for Git Hook
// -------------------------------------

gulp.task('lint:js:hook', function() {
    return lintJs(true);
});

// -------------------------------------
//   Task: Lint HTML
// -------------------------------------

gulp.task('lint:html', function() {
    return gulp.src(options.html.files)
        .pipe(plugins.htmlhint('.htmlhintrc'))
        .pipe(plugins.htmlhint.reporter());
});

// -------------------------------------
//   Task: Lint All (HTML, JS and Sass)
// -------------------------------------

gulp.task('lint:all', options.lint.tasks.all);

// -------------------------------------
//   Task: Watch
// -------------------------------------

gulp.task('watch', function() {
    var watchFiles = options.watch();

    watchFiles.forEach(function(watching) {
        gulp.watch(watching.files, watching.tasks);
    });
});

// -------------------------------------
//   Task: Git Hooks
// -------------------------------------

gulp.task('pre-commit', options.hooks['pre-commit']);